var app = angular.module('controllers', []);

app.controller('welcomeController', function($scope, $state, $ionicLoading, $rootScope, $window, $location, $timeout){
  console.log('welcome')

  //$state.go('welcome');
  $scope.show = function() {
    $ionicLoading.show({
     template: '<ion-spinner icon="bubbles" class="spinner-positive"></ion-spinner>'
    });
  };

  $scope.hide = function(){
    $ionicLoading.hide();
  };

  $scope.show();

  $timeout(function() {
    $ionicLoading.hide();
  }, 3000);

  if($rootScope.loaded == true){
    $rootScope.loaded = false;
    $timeout(function() {
      $window.location.reload(true);
    });
  }

  $scope.goToLogin = function(){
    $scope.show();
    $state.go('login');
  };

  $scope.test = function(){
    console.log('this is just a test');

  }

});

app.controller('loginController', function($ionicPlatform, $rootScope, $scope, $state, $http, $ionicPopup, $ionicLoading, enviroments, $cordovaCamera, $ionicLoading, $ionicModal, $timeout){
  console.log('login');
  console.log($rootScope.loaded)
  $scope.searchString = "";

  $ionicPlatform.onHardwareBackButton(function() {
    $rootScope.loaded = true;
    $state.go('welcome');
  });

  $scope.show = function() {
    $ionicLoading.show({
      template: '<ion-spinner icon="bubbles" class="spinner-positive"></ion-spinner>'
    });
  };

  $scope.hide = function(){
    $ionicLoading.hide();
  };

  $scope.show();

  $http.get('http://app.silicacloud.net:8080/PDD/UserList/0').then(function(response){
    console.log(response);
    $scope.employees = response.data;
    $scope.hide();
    $scope.showVisitType();

  });

  $ionicModal.fromTemplateUrl('templates/searchPopOver.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.launchSearch = function(){
    //console.log($scope.modal)
    $scope.modal.show();
    console.log(document.getElementById('search1').value);
    $scope.searchString = document.getElementById('search1').value;
    console.log($scope.searchString)
  };

  $scope.$on('modal.hidden', function() {
    console.log('gone')
  });

  $scope.closeModal = function() {
    $scope.modal.hide();
    $rootScope.loaded = true;
    $state.go('welcome');
  };

  $scope.destroyModal = function() {
    $scope.modal.remove();
    //$scope.modal = undefined;
  };

  $scope.showVisitType = function(){
    var myPopup = $ionicPopup.show({
    //template: '<input type="password" ng-model="data.wifi">',
    title: 'Reason of visit?',
    subTitle: 'Please select a visit reason',
    scope: $scope,
    buttons: [
      {
        text: 'Delivery',
        onTap: function(e) {
          $scope.visitType = 'delivery';
        }
       },
      {
        text: 'Meeting',
        type: 'button-positive',
        onTap: function(e) {
          $scope.visitType = 'meeting';
        }
      }
    ]
  });
  };

  $scope.attempVisit = function(employee){
    console.log(employee);
    console.log($scope.visitType)
    $scope.visitor = {};

    var visitPopup = $ionicPopup.show({
      template: '<input type="text" ng-model="visitor.name" style="height: 100%; text-align: center; font-size: 30px;">',
      title: '<div class="hide-on-keyboard-open">Visiting ' + employee.username +'<div>',
      subTitle: 'Please enter your name',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Save</b>',
          type: 'button-silica',
            onTap: function(e) {
              if (!$scope.visitor.name) {
                e.preventDefault();
              } else {
                return $scope.visitor.name;
              }
            }
          }
        ]
      });

      visitPopup.then(function(visitor){

        console.log(visitor)
        if(visitor){

          $scope.show();
          var image = {};

          if(employee.vCellno.substr(0, 1) == "0"){
            var cell = "+27" + employee.vCellno.substr(1, employee.vCellno.length);
            console.log(cell);
            employee.vCellno = cell;
          }else if(employee.vCellno.length < 10){
            var cell = "+27" + employee.vCellno;
            console.log(cell);
            employee.vCellno = cell;
          }

          var data = {
            image: "",
            username: visitor,
            vEmail: employee.vEmail,
            vCellno : employee.vCellno
          }

          if($scope.visitType === 'meeting'){
            $http.get('http://10.6.163.51:80/WebApi.Miscellaneous.Services/api/LabelPrinter?visitor='+ visitor +'&visiting=' + employee.username).then(function(response){
              console.log(response);
            })
          }

          $http.post('http://app.silicacloud.net:80/saintPete/visitorMail', data).then(function(response){
            console.log(response);
            //$scope.closeModal();

          });

          var message = "Your visitor "+ data.username +" has arrived at reception. Please collect your visitor.";
          //http://104.155.86.92/Communicate/SendLocalSMS/c3BuMTM3bHU2RzloaWgzMjIxUmsxZ0JqT05ZOHdkeGE6MDhSdU/
          //http://104.199.11.197/Communicate/SendLocalSMS/c3BuMTM3bHU2RzloaWgzMjIxUmsxZ0JqT05ZOHdkeGE6MDhSPP/
          //http://104.155.86.92/Communicate/SendLocalSMS/c3BuMTM3bHU2RzloaWgzMjIxUmsxZ0JqT05ZOHdkeGE6MDhSdU/
          var url = 'http://uatgry-bciapp01/WebApi.Miscellaneous.Services/api/SMS?uCellNumber='+ data.vCellno + '&uMessage=' + message;
          console.log(url);

          $http.get(url).then(function(response){
            $scope.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Great',
              template: '<div style="text-align: center; font-size: 20px;"> Your host has been contacted and will be with you in a moment<br/><br/> Please collect visitor\'s card and proceed to the client\'s waiting area<div>'
            });

            alertPopup.then(function(res) {
              $rootScope.loaded = true;
              $state.go('welcome');
            });
          }).catch(function(err){
            $scope.hide();
            var alertPopup = $ionicPopup.alert({
              title: 'Error',
              template: '<div style="text-align: center; font-size: 20px;"> Unable to send sms, please contact app admin<br/><br/> Please collect visitor\'s card and proceed to the client\'s waiting area<div>'
            });

            alertPopup.then(function(res) {
              $rootScope.loaded = true;
              $state.go('welcome');
            });
          });
        }
      });


  };
});

app.filter('searchFor', function(){
    return function(arr, searchString){
        if(!searchString){
            return arr;
        }
        var result = [];
        searchString = searchString.toLowerCase();
        angular.forEach(arr, function(item){
            if(item.username.toLowerCase().indexOf(searchString) !== -1 || item.vWorkno.substr(6, 4).toLowerCase().indexOf(searchString) !== -1){
            result.push(item);
        }
        });
        return result;
    };
});
