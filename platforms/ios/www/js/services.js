var app = angular.module('services', []);

app.factory('socket', function ($rootScope) {
  var socket = io.connect('http://127.0.0.1:3000');
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

app.factory('enviroments', function(){

  var enviroments = {};
  enviroments.gcp = 'http://app.silicacloud.net:8084'; //'http://104.154.226.177:80';
  enviroments.local = 'http://localhost:3000';

  return enviroments;
});
