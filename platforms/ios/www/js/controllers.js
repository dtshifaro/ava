var app = angular.module('controllers', []);

app.controller('welcomeController', function($scope, $state, $ionicLoading, $rootScope, $window, $location, $timeout){
  console.log('welcome')

  //$state.go('welcome');
  $scope.show = function() {
    $ionicLoading.show({
     template: '<ion-spinner icon="bubbles" class="spinner-positive"></ion-spinner>'
    });
  };

  $scope.hide = function(){
    $ionicLoading.hide();
  };

  $scope.show();

  $timeout(function() {
    $ionicLoading.hide();
  }, 3000);

  if($rootScope.loaded == true){
    $rootScope.loaded = false;
    $timeout(function() {
      $window.location.reload(true);
    });
  }

  $scope.goToLogin = function(){
    $scope.show();
    $state.go('login');
  };

  $scope.test = function(){
    console.log('this is just a test');

  }

});

app.controller('loginController', function($ionicPlatform, $rootScope, $scope, $state, $http, $ionicPopup, $ionicLoading, enviroments, $cordovaCamera, $ionicLoading, $ionicModal, $timeout){
  console.log('login');
  console.log($rootScope.loaded)
  $scope.searchString = "";

  $ionicPlatform.onHardwareBackButton(function() {
    $rootScope.loaded = true;
    $state.go('welcome');
  });

  $scope.show = function() {
    $ionicLoading.show({
      template: '<ion-spinner icon="bubbles" class="spinner-positive"></ion-spinner>'
    });
  };

  $scope.hide = function(){
    $ionicLoading.hide();
  };

  $scope.show();

  $http.get('http://app.silicacloud.net:8080/PDD/UserList/0').then(function(response){
    console.log(response);
    $scope.employees = response.data;
    $scope.hide();

  });

  $ionicModal.fromTemplateUrl('templates/searchPopOver.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.launchSearch = function(){
    //console.log($scope.modal)
    $scope.modal.show();
    console.log(document.getElementById('search1').value);
    $scope.searchString = document.getElementById('search1').value;
    console.log($scope.searchString)
  };

  $scope.$on('modal.hidden', function() {
    console.log('gone')
  });

  $scope.closeModal = function() {
    $scope.modal.hide();
    $rootScope.loaded = true;
    $state.go('welcome');
  };

  $scope.destroyModal = function() {
    $scope.modal.remove();
    //$scope.modal = undefined;
  };

  $scope.attempVisit = function(employee){
    console.log(employee);
    $scope.visitor = {};

    var visitPopup = $ionicPopup.show({
      template: '<input type="text" ng-model="visitor.name">',
      title: 'Visiting ' + employee.username,
      subTitle: 'Please enter your name',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Save</b>',
          type: 'button-silica',
            onTap: function(e) {
              if (!$scope.visitor.name) {
                e.preventDefault();
              } else {
                return $scope.visitor.name;
              }
            }
          }
        ]
      });


      visitPopup.then(function(visitor){
        console.log(visitor)
        if(visitor){

          $scope.timer = 4;

          var alertPopup1 = $ionicPopup.alert({
            title: 'SMILE!',
            scope: $scope,
            okText: 'NOW',
            template: '<strong>Time to take your picture In {{timer}}</strong><br><br><i class="icon custom-icon2 ion-happy"></i>'
          });

          $timeout(function(){
            $scope.timer -= 1;
            $timeout(function(){
              $scope.timer -= 1;
              $timeout(function(){
                $scope.timer -= 1;
                $timeout(function(){
                  $scope.timer -= 1;
                  alertPopup1.close();
                }, 1000);
              }, 1000);
            }, 1000);
          }, 1000);
        }

        $timeout(function(){

          if(visitor){
            var options = {
              quality: 50,
              cameraDirection: 1,
              destinationType: Camera.DestinationType.DATA_URL,
              sourceType: Camera.PictureSourceType.CAMERA,
              allowEdit: false,
              encodingType: Camera.EncodingType.JPEG,
              targetWidth: 100,
              targetHeight: 100,
              popoverOptions: CameraPopoverOptions,
              saveToPhotoAlbum: false,
              correctOrientation:true,

            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
              $scope.show();
              var image = {};
              image.src = "data:image/jpeg;base64," + imageData;

              if(employee.vCellno.substr(0, 1) == "0"){
                var cell = "+27" + employee.vCellno.substr(1, employee.vCellno.length);
                console.log(cell);
                employee.vCellno = cell;
              }

              var data = {
                image: "data:image/jpeg;base64," + imageData,
                username: visitor,
                vEmail: employee.vEmail,
                vCellno : employee.vCellno
              }

              $http.post('http://app.silicacloud.net:80/saintPete/visitorMail', data).then(function(response){
                console.log(response);
                //$scope.closeModal();
                $scope.hide();
                var alertPopup = $ionicPopup.alert({
                  title: 'Great',
                  template: 'Your host has been contacted and will be with you in a moment<br/> Please collect visitor\'s card and proceed to the client\'s waiting area'
                });

                alertPopup.then(function(res) {
                  $rootScope.loaded = true;
                  $state.go('welcome');
                });
              });
            }, function(err) {
              // error
            });
          }
        }, 4000);
      });

  };
});

app.filter('searchFor', function(){
    return function(arr, searchString){
        if(!searchString){
            return arr;
        }
        var result = [];
        searchString = searchString.toLowerCase();
        angular.forEach(arr, function(item){
            if(item.username.toLowerCase().indexOf(searchString) !== -1){
            result.push(item);
        }
        });
        return result;
    };
});
